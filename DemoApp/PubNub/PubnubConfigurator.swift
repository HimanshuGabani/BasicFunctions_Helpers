//
//  PubnubConfigurator.swift
//  DemoApp
//
//  Created by Himanshu Gabani on 29/04/21.
//  Copyright © 2020 Himanshu Gabani. All rights reserved.
//

//import PubNub
//
//class PubnubConfigurator: NSObject, PNEventsListener {
//
//    public static let shared = PubnubConfigurator()
//
//    private var clientPubNub = PubNub()
//
//    //MARK:- PubNub Event Track Delegate
//
//    // Handle new message from one of channels on which client has been subscribed.
//    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
//
//        // Handle new message stored in message.data.message
//        print("Received message: \(String(describing: message.data.message)) on channel \(message.data.channel) " +
//            "at \(message.data.timetoken)")
//
//        if ApplicationData.user.id != nil || ApplicationData.user.id != "" {
//            if message.data.publisher == ApplicationData.user.id! {
//                return
//            }
//        }
//        if message.data.message != nil && message.data.message is NSDictionary {
//
//            if (message.data.message as! NSDictionary).value(forKey: "declaration") != nil && (message.data.message as! NSDictionary).value(forKey: "declaration") is String {
//
//                let status_key = (message.data.message as! NSDictionary).value(forKey: "declaration") as! String
//
//                if status_key == ORDER_RECEIVED {
//                    //Order Received
//                    NotificationCenter.default.post(name: Notification.Name(ORDER_UPDATE), object: nil, userInfo: (message.data.message as! [AnyHashable : Any]))
//                } else if status_key == ORDER_STATUS_CHANGED {
//                    //Order status changed
//                    NotificationCenter.default.post(name: Notification.Name(ORDER_UPDATE), object: nil, userInfo: (message.data.message as! [AnyHashable : Any]))
//                } else if status_key == PAYMENT_RECEIVED {
//                    //payment received
//                    NotificationCenter.default.post(name: Notification.Name(ORDER_UPDATE), object: nil, userInfo: (message.data.message as! [AnyHashable : Any]))
//                }
//            }
//        }
//        if message.data.channel != message.data.subscription {
//            // Message has been received on channel group stored in message.data.subscription.
//        } else {
//            // Message has been received on channel stored in message.data.channel.
//        }
//    }
//
//    // New presence event handling.
//    func client(_ client: PubNub, didReceivePresenceEvent event: PNPresenceEventResult) {
//
//        // Handle presence event event.data.presenceEvent (one of: join, leave, timeout, state-change).
//        if event.data.channel != event.data.subscription {
//            // Presence event has been received on channel group stored in event.data.subscription.
//        } else {
//            // Presence event has been received on channel stored in event.data.channel.
//        }
//        if event.data.presenceEvent != "state-change" {
//            print("\(event.data.presence.uuid ?? "") \"\(event.data.presenceEvent)'ed\"\n" +
//                "at: \(event.data.presence.timetoken) on \(event.data.channel) " +
//                "(Occupancy: \(event.data.presence.occupancy))");
//        } else {
//            print("\(event.data.presence.uuid ?? "") changed state at: " +
//                "\(event.data.presence.timetoken) on \(event.data.channel) to:\n" +
//                "\(event.data.presence.state ?? [:])");
//        }
//    }
//
//    // Handle subscription status change.
//    func client(_ client: PubNub, didReceive status: PNStatus) {
//
//        if status.operation == .subscribeOperation {
//
//            // Check whether received information about successful subscription or restore.
//            if status.category == .PNConnectedCategory || status.category == .PNReconnectedCategory {
//
//                let subscribeStatus: PNSubscribeStatus = status as! PNSubscribeStatus
//                if subscribeStatus.category == .PNConnectedCategory {
//
//                    print(subscribeStatus.lastTimeToken)
//                    // This is expected for a subscribe, this means there is no error or issue whatsoever.
//                } else {
//                    print(subscribeStatus.lastTimeToken)
//                    /**
//                     This usually occurs if subscribe temporarily fails but reconnects. This means there was
//                     an error but there is no longer any issue.
//                     */
//                }
//                kAppDelegate.isPubNubConnected = true
//                NotificationCenter.default.post(name: NOTIFICATION_CONNECTED_PUBNUB , object: nil)
//            } else if status.category == .PNUnexpectedDisconnectCategory {
//                /**
//                 This is usually an issue with the internet connection, this is an error, handle
//                 appropriately retry will be called automatically.
//                 */
//                kAppDelegate.isPubNubConnected = false
//                NotificationCenter.default.post(name: NOTIFICATION_DISCONNECTED_PUBNUB , object: nil)
//            }
//                // Looks like some kind of issues happened while client tried to subscribe or disconnected from
//                // network.
//            else {
//                kAppDelegate.isPubNubConnected = false
//                NotificationCenter.default.post(name: NOTIFICATION_DISCONNECTED_PUBNUB , object: nil)
//                let errorStatus: PNErrorStatus = status as! PNErrorStatus
//                if errorStatus.category == .PNAccessDeniedCategory {
//                    /**
//                     This means that PAM does allow this client to subscribe to this channel and channel group
//                     configuration. This is another explicit error.
//                     */
//                } else {
//                    /**
//                     More errors can be directly specified by creating explicit cases for other error categories
//                     of `PNStatusCategory` such as: `PNDecryptionErrorCategory`,
//                     `PNMalformedFilterExpressionCategory`, `PNMalformedResponseCategory`, `PNTimeoutCategory`
//                     or `PNNetworkIssuesCategory`
//                     */
//                }
//            }
//        }
//    }
//
//    //MARK:- PubNub Initialize, Subscribe/Unsubscribe
//
//    func inititalizePubNub(publishKey: String, subscribeKey: String, cipherKey: String) {
//        // Initialize and configure PubNub client instance
//
//        let configuration = PNConfiguration(publishKey: publishKey, subscribeKey: subscribeKey)
//        configuration.uuid = ApplicationData.user.id!
//        self.clientPubNub = PubNub.clientWithConfiguration(configuration)
//        self.clientPubNub.addListener(self)
//    }
//
//    func subscribeChannel(arrChannel: [String]) {
//        // Subscribe to demo channel with presence observation
//        self.clientPubNub.subscribeToChannels(arrChannel, withPresence: true)
//    }
//
//    func unSubscribeChannel() {
//        // Subscribe to demo channel with presence observation
//        self.clientPubNub.unsubscribeFromAll()
//    }
//
//    //MARK:- PubNub Publish Message
//
//    func postMessageOverChannel(message: Any) {
//        // Select last object from list of channels and send message to it.
//        if clientPubNub.channels().count == 0 {
//            return
//        }
//        let targetChannel = clientPubNub.channels().last!
//        clientPubNub.publish(message, toChannel: targetChannel, compressed: false, withCompletion: { (publishStatus) -> Void in
//
//            if !publishStatus.isError {
//                // Message successfully published to specified channel.
//            } else {
//                /**
//                 Handle message publish error. Check 'category' property to find out
//                 possible reason because of which request did fail.
//                 Review 'errorData' property (which has PNErrorData data type) of status
//                 object to get additional information about issue.
//                 Request can be resent using: publishStatus.retry()
//                 */
//            }
//        })
//    }
//
//    func createOrderStatusChangeMessage(orderId: Int, locationId: String, status: String) -> Any {
//        /**
//         {
//             "declaration": "order_status_changed",
//             "payload": {
//                 "order_id": 410,
//                 "status": "preparing",
//                 "location_id": "0ebc63b0-e0aa-46ce-98ab-e3af6144a08b"
//             }
//         }
//         */
//        let payload = ["order_id": orderId, "status": status, "location_id": locationId] as [String : Any]
//        let dict = ["declaration":ORDER_STATUS_CHANGED, "payload": payload] as [String : Any]
//        return dict
//    }
//
//
//    func createPaymentReceiveMessage(orderId: Int, locationId: String) -> Any {
//        /**
//         {
//             "declaration": "payment_received",
//             "payload": {
//                 "order_id": 410,
//                 "status": true,
//                 "location_id": "0ebc63b0-e0aa-46ce-98ab-e3af6144a08b"
//             }
//         }
//         */
//        let payload = ["order_id": orderId, "status": true, "location_id": locationId] as [String : Any]
//        let dict = ["declaration":PAYMENT_RECEIVED, "payload": payload] as [String : Any]
//        return dict
//    }
//
//}
