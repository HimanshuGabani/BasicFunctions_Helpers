//
//  ApplicationData.swift
//  DemoApp
//
//  Created by Himanshu Gabani on 11/10/19.
//  Copyright © 2019 Himanshu Gabani. All rights reserved.
//

import UIKit

//This singleton class is used to store application level data.
class ApplicationData: NSObject {
    
    // A Singleton instance
    static let shared = ApplicationData()
    
//    static var user: ModelType {
//        get {
//            return UserDefaultsMethods.getCustomObject(key: AppConstants.UserDefaultKeys.UserInfoModelKey) as? ModelType ?? ModelType()
//        }
//    }
    
    // returns Current Time Zone
    static var localTimeZoneName: String { return TimeZone.current.identifier }
    
    //retun device Name
    static var deviceName: String {
        get {
            return UIDevice.current.localizedModel
        }
    }
    
    //retun device version
    static var deviceVersion: String {
        get {
            return UIDevice.current.systemVersion
        }
    }
    
    static var deviceId: String {
        get {
            return UIDevice.current.identifierForVendor!.uuidString
        }
    }
    
    //return UUID
    func getUUID() -> String {
        return UUID().uuidString
    }
    
    //MARK: Private Methods
    
    func logoutUser(isShowConfirmation:Bool) {
        if isShowConfirmation {
            CommonMethods.showAlertWithTwoButtonAction(title: "", message: StringConstants.MessageConstant.LogoutMsg, attributedMessage: nil, buttonTitle1: StringConstants.ButtonConstant.kNo, buttonTitle2: StringConstants.ButtonConstant.kYes, onButton1Click: { }) {
                self.logout(isShowConfirmation: isShowConfirmation)
            }
        } else {
            self.logout(isShowConfirmation: isShowConfirmation)
        }
    }
    
    private func logout(isShowConfirmation:Bool) {
        UserDefaultsMethods.removeAllKeyFromDefault()
        let vc = StoryBoard.Data.Main.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let nav = UINavigationController(rootViewController: vc)
        nav.setNavigationBarHidden(true, animated: true)
        kAppDelegate.window?.rootViewController = nav
        kAppDelegate.window?.makeKeyAndVisible()
        
        UIView.transition(with: kAppDelegate.window!,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: nil,
                          completion: nil)
    }
}

