//
//  APIManager.swift
//  DemoApp
//
//  Created by Himanshu Gabani on 11/10/19.
//  Copyright © 2019 Himanshu Gabani. All rights reserved.
//

import UIKit

class APIManager: NSObject {
    
    //MARK: Shared Instance
    static let shared = APIManager()
    
    // A network reachability instance
    let networkReachability = NetworkReachabilityManager()
    
    //MARK: - Constant
    struct Constants {
        
        struct URLs {
            
            private static let API_VER        = "/api/v1/"
                        
            //MARK: Auth
            static let LOGIN_USER               = "\(API_VER)auth/login/local"
        }
        
        struct ResponseKey {
            
            static let code                         = "status_code"
            static let data                         = "data"
            static let message                      = "msg"
            static let list                         = "list"
        }
        
        struct ResponseCode {
            
            static let kArrSuccessCode              = [200,201]
            static let kErrorCode                   = 400
            static let kUnAuthorizeCode             = 401
            static let kNotFound                    = 404
        }
    }
    
    // Initialize
    private override init() {
        super.init()
        self.networkReachability?.startListening { status in
            
        }
        setIndicatorViewDefaults()
    }
    
    // MARK: - Indicator view
    private func setIndicatorViewDefaults() {
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballTrianglePath
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 40, height: 40)
        NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE_FONT = UIFont.boldSystemFont(ofSize: 17)
    }
    
    /// show indicator before network request
    func showIndicator(_ message:String = "", stopAfter: Double = 0.0) {
        
        let activityData = ActivityData(message: message)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        if stopAfter > 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + stopAfter) {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
    }
    
    // Stop Indicator Manually
    func stopIndicator() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    func isNetworkAvailable() -> Bool {
        guard (networkReachability?.isReachable)! else {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            return false
        }
        
        return true
    }
    
    //MARK: Custom Headers
    
    var authHeaders: HTTPHeaders {
        get {
            var headers : HTTPHeaders = [:]
            
            headers["Content-Type"] = "application/json"
//            if let token = ApplicationData.user.token,token.count > 0 {
//                headers["auth-token"] = token
//            }
            headers["api-token"] = SERVER_API_TOKEN
            return headers
        }
    }

    // MARK:- Request Promise
    func makeRequest(server: String, endPoint: String,method: Alamofire.HTTPMethod = .get, parameters: Parameters? = nil,encoding: ParameterEncoding, headers: HTTPHeaders?) -> Promise<[String:Any]> {
        return Promise { res in
            //hide keyboard
            UIViewController.current()?.view.endEditing(false)
           
            //Check Network
            if !self.isNetworkAvailable() {
                //hide indicator
                self.stopIndicator()
                res.reject(CustomError.init(title: kAppName, description: NO_INTERNET_CONNECTION, code: Constants.ResponseCode.kErrorCode))
                return
            }
            
            //Make Final URL
            let finalURLString = "\(server)\(endPoint)"
            
            //Check For Valid URL
            guard let finalURL = URL(string: finalURLString) else {
                //hide indicator
                self.stopIndicator()
                res.reject(CustomError.init(title: kAppName, description: ERROR_NOT_EXPECTED, code: Constants.ResponseCode.kErrorCode))
                return
            }
            
            // Network request
            AF.request(finalURL, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : AFDataResponse<Any>) in
                
                //hide indicator
                self.stopIndicator()
                
                // check result is success
                switch response.result {
                case let .success(value):
                    
                    //response payload
                    if let responseObject = value as? [String: Any]{
                        
                        //print response
                        print(responseObject.printJson())
                        
                        //status code
                        let status_code = response.response?.statusCode ?? 0
                        
                        //message
                        var msg = responseObject[Constants.ResponseKey.message] as? String ?? ""
                        
                        //check if code is error then get msg from payload
                        if status_code == Constants.ResponseCode.kErrorCode{
                            
                            if let objPayload = responseObject["payload"] as? [String:Any],let arrDetail = objPayload["details"] as? [[String:Any]],let payloadMsg = arrDetail.first?["message"] as? String {
                                msg = payloadMsg
                            }
                        }
                        
                        //check token expired logout user if true
                        guard status_code != Constants.ResponseCode.kUnAuthorizeCode else {
                            
                            res.reject(CustomError.init(title: kAppName, description: msg, code: status_code))
                            ApplicationData.shared.logoutUser(isShowConfirmation: false)
                            return
                        }
                        
                        //check status code
                        if Constants.ResponseCode.kArrSuccessCode.contains(status_code) {
                            res.fulfill(responseObject)
                        } else {
                            res.reject(CustomError.init(title: kAppName, description: msg, code: status_code))
                        }
                    } else {
                        var msgMain = ""
                        if let msg = response.error?.localizedDescription,msg.count > 0 {
                            msgMain = msg
                        } else {
                            msgMain = NULL_RESPONSE
                        }
                        res.reject(CustomError.init(title: kAppName, description: msgMain, code: Constants.ResponseCode.kErrorCode))
                    }
                    
                case let .failure(error):
                    res.reject(CustomError.init(title: kAppName, description: error.localizedDescription, code: Constants.ResponseCode.kErrorCode))
                    return
                }
            }
        }
    }
    
    //Promise with dynamic return types
    //    func makeRequest<T>(endPoint:String,method: HTTPMethod = .get, parameters: Parameters? = nil,encoding:ParameterEncoding, headers: HTTPHeaders? = nil) -> Promise<T> {
    //        return Promise<T> { resolve, reject in
    //
    //            //Check Network
    //            if !self.isNetworkAvailable() {
    //
    //                reject(CustomError.init(title: "title", description: "desc", code: 401))
    //            }
    //
    //            //Final URL
    //            let finalURLString: String = "\(AppConstant.SERVER)\(endPoint)"
    //            guard let finalURL = URL(string: finalURLString) else{
    //                reject(CustomError.init(title: "", description: "", code: 401))
    //                return
    //            }
    //
    //            // Network request
    //            Alamofire.request(finalURL, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response: DataResponse<Any>) in
    //
    //                // check result is success
    //                guard response.result.isSuccess else {
    //                    reject(CustomError.init(title: "", description: "", code: 401))
    //                    return
    //                }
    //
    //                if let responseObject = response.result.value as? T{
    //
    //                    resolve(responseObject)
    //                }else{
    //
    //                    reject(CustomError.init(title: "Invalid type", description: "", code: 401))
    //                }
    //
    //            }
    //        }
    //
    //    }
    
    ////MARK: Image Upload API
    
    //    func callAPIForImageUpload(arrImage:[UIImage],completion: @escaping (_ arrURL: [String]) -> Void){
    //
    //        var arrPromises = [Promise<String>]()
    //        for img in arrImage{
    //            let imgData = img.jpegData(compressionQuality: 0.1)
    //            arrPromises.append(self.makeRequestForImageUpload(image: UIImage.init(data: imgData!)!))
    //        }
    //
    //        APIManager.shared.showIndicator("Uploading Images", stopAfter: 0.0)
    //        firstly {
    //
    //            when(resolved: arrPromises)
    //
    //        }.done { arrResult in
    //
    //            var arrURL = [String]()
    //            for res in arrResult{
    //
    //                switch(res){
    //                case .fulfilled(let value) :
    //                    arrURL.append(value)
    //                    break
    //                default : break
    //                }
    //            }
    //            APIManager.shared.stopIndicator()
    //            completion(arrURL)
    //        }.catch { error in
    //
    //            Utilities.showAlertView(title: kAppName, message: error.localizedDescription)
    //            APIManager.shared.stopIndicator()
    //        }
    //
    //    }
    
    //    fileprivate func makeRequestForImageUpload(image:UIImage) -> Promise<String> {
    //        return Promise { pmResult in
    //
    //            var param = [String:Any]()
    //            //            param["business_id"] = 1313
    //            //            param["folder_id"] = 489
    //            //            param["folder"] = "5d4be1ddfe7d582ebadcf30f" // Commented By HD Reference By DK Given By CV observer MD
    //
    //            param["description"] = kAppName
    //            param["content_type"] = "image/jpeg"
    //            param["name"] = String(format: "iOS_IMG_%@", Date.init() as CVarArg).replacingOccurrences(of: " ", with: "")
    //
    //            var header = [String:String]()
    //            header["api-token"] = APIManager.Constants.RESOURCE_TOKEN
    //
    //            APIManager.shared.makeRequest(endPoint: "", method: .post, parameters: param, encoding: JSONEncoding.default, headers: header,isShowLoader: false,fullURL: APIManager.Constants.URLs.UPLOAD_IMAGE).done { (res) in
    //
    //                DispatchQueue.main.async {
    //
    //                    print(res)
    //
    //                    if let dictPayload = res["payload"] as? [String:Any]{
    //
    //                        //if let url = dictPayload["pre_signed_url"] as? String{
    //                        if let url = dictPayload["upload_url"] as? String{
    //                            let imgUrl = URL.init(string:url)
    //                            var absoluteURL = imgUrl?.absoluteString.replacingOccurrences(of: (imgUrl?.query)!, with: "")
    //                            absoluteURL = absoluteURL?.replacingOccurrences(of: "?", with: "")
    //
    //
    //                            //Image upload
    //                            let header = ["Content-Type":"image/jpeg"]
    //                            if let image = image.pngData(){
    //
    //                                Alamofire.upload(image, to: url, method: .put, headers: header).response { (res) in
    //
    //                                    if res.response?.statusCode == 200 {
    //
    //                                        pmResult.fulfill(absoluteURL ?? "")
    //                                    }else{
    //
    //                                        let errorTemp = NSError(domain:res.error?.localizedDescription ?? "Something went wrong please try again later", code: res.response?.statusCode ?? 401, userInfo:nil)
    //                                        pmResult.reject(errorTemp)
    //                                    }
    //
    //                                }
    //                            }
    //
    //
    //                        }
    //
    //                    }
    //
    //                }
    //            }
    //
    //            .catch { (err) in
    //
    //                Utilities.showAlertView(title: kAppName, message: err.localizedDescription)
    //            }
    //
    //
    //        }
    //
    //    }
    
}


protocol OurErrorProtocol: LocalizedError {
    
    var title: String? { get }
    var code: Int { get }
}

struct CustomError: OurErrorProtocol {
    
    var title: String?
    var code: Int
    var errorDescription: String? { return _description }
    var failureReason: String? { return _description }
    
    private var _description: String
    
    init(title: String?, description: String, code: Int) {
        self.title = title ?? "Error"
        self._description = description
        self.code = code
    }
}

