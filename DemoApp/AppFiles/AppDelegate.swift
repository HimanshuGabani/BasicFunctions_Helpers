//
//  AppDelegate.swift
//  DemoApp
//
//  Created by Himanshu Gabani on 13/04/21.
//  Copyright © 2020 Himanshu Gabani. All rights reserved.
//

import UIKit
import UserNotifications
import AVFoundation
import MediaPlayer

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    //MARK:- Variable Declaration

    var window: UIWindow?
    var launchPushInfo : [AnyHashable : Any]!
    var isFromLaunch : Bool = false

    //MARK:- Application Delegate Methods

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if launchOptions != nil {
            if ((launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification]) != nil) {
                let pushinfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as! [AnyHashable : Any]
                self.launchPushInfo = pushinfo
                self.isFromLaunch = true
            }
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()

        IQKeyboardManager.shared.enable = true
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

//MARK:- Private Methods
extension AppDelegate{
    
    func setDefaultSettings() {
        let volumeView = MPVolumeView()
        if let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider {
            //Double(slider.value) id value of volume
        }
        //KEEP screen on or off while app is open
//        if let item = UserDefaultsMethods.getBool(key: KEEP_SCREEN_ON) {
//            UIApplication.shared.isIdleTimerDisabled = item
//        } else {
//            UIApplication.shared.isIdleTimerDisabled = false
//        }
    }

    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        else if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        else if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
//MARK:- Notification Setup

extension AppDelegate : UNUserNotificationCenterDelegate {

    //MARK: - Push Notification Delegate
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
    }
    
      func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // 1
        let userInfo = response.notification.request.content.userInfo
        
        print("=================")
        print(userInfo.printJson())
        print("=================")
        navigateToRelatedPageByNotification(userInfo: userInfo,isFromLaunch: self.isFromLaunch)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("=================")
        print(userInfo.printJson())
        print("=================")
        navigateToRelatedPageByNotification(userInfo: userInfo,isFromLaunch: self.isFromLaunch)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("=================")
        print(userInfo.printJson())
        print("=================")
        navigateToRelatedPageByNotification(userInfo: userInfo,isFromLaunch: self.isFromLaunch)
    }

    func navigateToRelatedPageByNotification(userInfo: [AnyHashable : Any] , isFromLaunch : Bool) {
        
    }
        
    func registerPush(isLiveSDK: Bool) {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            
            center.delegate = self
            center.requestAuthorization(options: [.alert, .badge, .sound]) { (success, error) in
                if error == nil {
                    if success == true {
                        // print("Permission granted")
                    } else {
                        // print("Permission not granted")
                    }
                }
            }
        } else {
            //self.createUserNotificationSettings()
        }
        
    }
    
}

extension MPVolumeView {
  static func setVolume(_ volume: Float) {
    let volumeView = MPVolumeView()
    let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider

    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.00) {
      slider?.value = volume
    }
  }
}
