//
//  ClsSimplePushAlert.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 3, 2019

import Foundation 
import Gloss

//MARK: - ClsSimplePushAlert
public class ClsSimplePushAlert: Glossy {
    public var body : String!
    public var title : String!

	//MARK: Default Initializer 
	init()
	{
        body = ""
        title = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let body : String = "body" <~~ json {
            self.body = body
        }else{
            self.body = ""
        }
        if let title : String = "title" <~~ json {
            self.title = title
        }else{
            self.title = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "body" ~~> body,
        "title" ~~> title,
		])
	}

}
