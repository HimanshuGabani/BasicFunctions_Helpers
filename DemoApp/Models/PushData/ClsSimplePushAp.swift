//
//  ClsSimplePushAp.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 3, 2019

import Foundation 
import Gloss

//MARK: - ClsSimplePushAp
public class ClsSimplePushAp: Glossy {
    public var alert : ClsSimplePushAlert!
    public var badge : Int!
    public var sound : String!

	//MARK: Default Initializer 
	init()
	{
        alert = ClsSimplePushAlert()
        badge = 0
        sound = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let alert : ClsSimplePushAlert = "alert" <~~ json {
            self.alert = alert
        }else{
            self.alert = ClsSimplePushAlert()
        }
        if let badge : Int = "badge" <~~ json {
            self.badge = badge
        }else{
            self.badge = 0
        }
        if let sound : String = "sound" <~~ json {
            self.sound = sound
        }else{
            self.sound = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "alert" ~~> alert,
        "badge" ~~> badge,
        "sound" ~~> sound,
		])
	}

}
