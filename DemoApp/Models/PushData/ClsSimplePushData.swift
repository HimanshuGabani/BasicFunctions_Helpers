//
//  ClsSimplePushData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 3, 2019

import Foundation 
import Gloss

//MARK: - ClsSimplePushData
public class ClsSimplePushData: Glossy {
    public var aps : ClsSimplePushAp!
    public var badge : String!
    public var eventId : String!
    public var gcmmessageId : String!
    public var googlecae : String!
    public var isSdkNotification : String!
    public var payload : String!
    public var pushId : String!
    public var type : String!
    public var userId : String!

	//MARK: Default Initializer 
	init()
	{
        aps = ClsSimplePushAp()
        badge = ""
        eventId = ""
        gcmmessageId = ""
        googlecae = ""
        isSdkNotification = ""
        payload = ""
        pushId = ""
        type = ""
        userId = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let aps : ClsSimplePushAp = "aps" <~~ json {
            self.aps = aps
        }else{
            self.aps = ClsSimplePushAp()
        }
        if let badge : String = "badge" <~~ json {
            self.badge = badge
        }else{
            self.badge = ""
        }
        if let eventId : String = "eventId" <~~ json {
            self.eventId = eventId
        }else{
            self.eventId = ""
        }
        if let gcmmessageId : String = "gcm.message_id" <~~ json {
            self.gcmmessageId = gcmmessageId
        }else{
            self.gcmmessageId = ""
        }
        if let googlecae : String = "google.c.a.e" <~~ json {
            self.googlecae = googlecae
        }else{
            self.googlecae = ""
        }
        if let isSdkNotification : String = "isSdkNotification" <~~ json {
            self.isSdkNotification = isSdkNotification
        }else{
            self.isSdkNotification = ""
        }
        if let payload : String = "payload" <~~ json {
            self.payload = payload
        }else{
            self.payload = ""
        }
        if let pushId : String = "pushId" <~~ json {
            self.pushId = pushId
        }else{
            self.pushId = ""
        }
        if let type : String = "type" <~~ json {
            self.type = type
        }else{
            self.type = ""
        }
        if let userId : String = "userId" <~~ json {
            self.userId = userId
        }else{
            self.userId = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "aps" ~~> aps,
        "badge" ~~> badge,
        "eventId" ~~> eventId,
        "gcm.message_id" ~~> gcmmessageId,
        "google.c.a.e" ~~> googlecae,
        "isSdkNotification" ~~> isSdkNotification,
        "payload" ~~> payload,
        "pushId" ~~> pushId,
        "type" ~~> type,
        "userId" ~~> userId,
		])
	}

}
