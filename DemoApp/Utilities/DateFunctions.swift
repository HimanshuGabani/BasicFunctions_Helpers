//
//  DateFunctions.swift
//
//  Created by Apple on 04/10/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit


class DateFunctions: NSObject {
    
    static var localTimeZoneIdentifier: String { return TimeZone.current.identifier }

    struct DateFormat {
        
        static let ISOFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        static let ServerFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        static let YYYYMMDD = "yyyy-MM-dd"
        static let OrderDetailFormat = "MMM d yyyy"
        static let TransportDate = "MMM d yyyy, hh:mm a"
        static let TimeFormat = "hh:mm a"
        static let OrderReceiptFormat = "hh:mm a, MMM dd yyyy"
        static let ItemDetailFormat = "MMM d, yyyy"
        static let TimeFormatHH = "HH:mm"
        static let HHmmss = "HH:mm:ss"
        static let UpcommingOrder = "EEE, MMM d"
        static let CSVDate = "MMM d yyyy hh:mm a"
        static let CSVFileDate = "yyyyMMdd_HHMMSS"
        static let EarningReportReqDate = "yyyy-MM-dd HH:mm:ss"
    }
        
    class func date(from dateString: String, format: String, isCurrentTimeZone: Bool) -> Date? {
        let dateFormat = DateFormatter()
        dateFormat.setLocale()
        dateFormat.dateFormat = format
        if isCurrentTimeZone {
            dateFormat.timeZone = .current
        } else {
            dateFormat.timeZone = TimeZone(abbreviation: "UTC")
        }
        let date: Date? = dateFormat.date(from: dateString)
        return date
    }
    
    class func string(from date: Date, format: String, isCurrentTimeZone: Bool) -> String {
        let formatter = DateFormatter()
        formatter.setLocale()
        if isCurrentTimeZone {
            formatter.timeZone = NSTimeZone.local
        } else {
            formatter.timeZone = TimeZone(abbreviation: "UTC")
        }
        formatter.dateFormat = format
        let dateString: String = formatter.string(from: date)
        return dateString
    }
    
    class func stringfromDate(from_date date: Date?, formate: String, isCurrentTimeZone:Bool) -> String? {
        if let dt = date {
            let formatter = DateFormatter()
            formatter.setLocale()
            formatter.dateFormat = formate
            if isCurrentTimeZone {
                formatter.timeZone = TimeZone.current
            } else {
                formatter.timeZone = TimeZone(abbreviation: "UTC")
            }
            return formatter.string(from: dt)
        }
        return nil
    }
    
    class func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat

        return dateFormatter.string(from: dt!)
    }

    class func stringDateFromFormatter(date:String, format: String) -> String {
        if date == "" {
            return ""
        }
        let dateFormatter = DateFormatter()
                
        dateFormatter.dateFormat = DateFormat.ISOFormat//8601(.DateTimeMilliSec)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormatter.date(from: date)
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: dt!)
    }
    
    class func stringDOBDateFromFormatter(date:String, format: String) -> String {
        if date == "" {
            return ""
        }
        let dateFormatter = DateFormatter()
                
        dateFormatter.dateFormat = DateFormat.ISOFormat//8601(.DateTimeMilliSec)
        let dt = dateFormatter.date(from: date)
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: dt!)
    }
}

//MARK: New Functions
extension DateFunctions {

    class func getCurrentDateInDashboardFormat() -> String {
        let formatter = DateFormatter()
        formatter.setLocale()
        formatter.dateFormat = "EEEE\nMMM d, yyyy"
        return formatter.string(from: Date())
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}

extension String {
    func ISOToLocalDateStr(outputFormat:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFunctions.DateFormat.ISOFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        guard let date = dateFormatter.date(from: self) else {
            return self
        }

        dateFormatter.dateFormat = outputFormat
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: date)
    }
}
