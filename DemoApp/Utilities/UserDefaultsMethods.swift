//
//  UserDefaultsMethods.swift
//
//  Created by Apple on 24/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class UserDefaultsMethods: NSObject {
    
    
    // MARK: - String Functions
    class func setString(value:String,key:String)
    {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func getString(key:String) -> String?
    {
        let value : String? = UserDefaults.standard.object(forKey: key) as? String
        return value
    }
    
    // MARK: - Int Functions
    class func setInt(value:Int,key:String)
    {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func getInt(key:String) -> Int?
    {
        let value : Int? = UserDefaults.standard.object(forKey: key) as? Int
        return value
    }
    
    // MARK: - Bool Functions
    class func setBool(value:Bool,key:String)
    {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func getBool(key:String) -> Bool?
    {
        let value : Bool? = UserDefaults.standard.value(forKey: key) as? Bool
        return value
    }
    
    // MARK: - Float Functions
    class func setFloat(value:Float,key:String)
    {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func getFloat(key:String) -> Float?
    {
        let value : Float? = UserDefaults.standard.object(forKey: key) as? Float
        return value
    }
    
    // MARK: - Double Functions
    class func setDouble(value:Double,key:String)
    {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func getFloat(key:String) -> Double?
    {
        let value : Double? = UserDefaults.standard.object(forKey: key) as? Double
        return value
    }
    
    
    // MARK: - MutableArray Functions
    class func setMutableArray(value:NSMutableArray,key:String)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: value)
        UserDefaults.standard.set(data, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func getMutableArray(key:String) -> NSMutableArray?
    {
        let data = UserDefaults.standard.object(forKey: key) as? NSData
        if data == nil {
            return nil
        }
        let value : NSMutableArray? = NSKeyedUnarchiver.unarchiveObject(with: data! as Data) as? NSMutableArray
        return value
    }
    
    
    // MARK: - MutableDict Functions
    /*class func setMutableDictonary(value:NSMutableDictionary,key:String)
     {
     let data = NSKeyedArchiver.archivedDataWithRootObject(value)
     UserDefaults.standard.setObject(data, forKey: key)
     UserDefaults.standard.synchronize()
     }
     
     class func getMutableDictonary(key:String) -> NSMutableDictionary?
     {
     let value : NSMutableDictionary? = UserDefaults.standard.objectForKey(key) as? NSMutableDictionary
     return value
     }*/
    
    
    // MARK: - Custom object Functions
    class func setCustomObject(value:AnyObject,key:String)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: value)
        UserDefaults.standard.set(data, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func getCustomObject(key:String) -> Any?
    {
        let data = UserDefaults.standard.object(forKey: key) as? NSData
        if data == nil
        {
            return nil
        }
        let value = NSKeyedUnarchiver.unarchiveObject(with: data! as Data)
        return value
    }
    
    class func setImageObject(value:UIImage,key:String)
    {
        UserDefaults.standard.set(value.pngData(), forKey: key)
        UserDefaults.standard.synchronize()
    }
    class func getImageObject(key:String) -> UIImage
    {
        let data = UserDefaults.standard.object(forKey: key) as? Data
        if data == nil
        {
            let img = UIImage.init()
            img.accessibilityIdentifier = "nil"
            return img
        }
        return UIImage.init(data: data ?? Data()) ?? UIImage()
    }
    
    class func removeObjectForKey(_ objectKey: String) {
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: objectKey)
        defaults.synchronize()
    }
    
    class func removeAllKeyFromDefault(){
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    }
    
}
