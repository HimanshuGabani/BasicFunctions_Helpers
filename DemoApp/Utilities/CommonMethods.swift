//
//  Utilities.swift


import UIKit
import MapKit

class CommonMethods: NSObject {
    
    class func topViewController() -> UIViewController {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        // topController should now be your topmost view controller
        }
        return kAppDelegate.window!.rootViewController!
    }
    
    //MARK:- JSON
    static func jsonToString(json: Any) -> String? {
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            return convertedString
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    
    static func objectFromJsonString(str:String) -> Any?{
        
        let jsonString = str
        let jsonData = jsonString.data(using: .utf8)
        let dict = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)
        return dict
    }
    
    class func convertToDictionary(text: String) -> [String: AnyObject]? {
        guard let data = text.data(using: .utf8) else {
            return ["code":400 as AnyObject]
        }
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
        } catch {
        }
        return ["code":400 as AnyObject]
    }
    
    //MARK:- UIImage Setup
    
    class func sizeOfImage(image:UIImage ,inAspectFitImageView imageview:UIImageView,view : UIView ) -> CGSize {
        
        //imageview.contentMode = UIViewContentMode.scaleAspectFit
        let imageViewWidth = imageview.bounds.size.width //view.frame.size.width
        let imageViewHeight = imageview.bounds.size.height
        
        let imageWidth = image.size.width
        let imageHeight = image.size.height
        
        let scaleFactor = max(imageViewWidth/imageWidth,imageViewHeight/imageHeight)
        
        return CGSize(width: image.size.width * scaleFactor, height: image.size.height * scaleFactor)
    }
    
    class func sizeOfImageWithHeightWidth(height:CGFloat,width : CGFloat ,inAspectFitImageView imageview:UIImageView , isWidthFixed : Bool = true) -> CGSize {
        
        imageview.contentMode = UIView.ContentMode.scaleAspectFit
        let imageViewWidth = isWidthFixed ?  imageview.bounds.size.width : UIScreen.main.bounds.size.width
        let imageViewHeight = imageview.bounds.size.height
        
        let imageWidth = width
        let imageHeight = height
        
        let scaleFactor = isWidthFixed ? max(imageViewWidth/imageWidth,imageViewHeight/imageHeight) : min(imageViewWidth/imageWidth,imageViewHeight/imageHeight)
        
        let s = CGSize(width: width * scaleFactor, height: height * scaleFactor)
        
        return s
    }
    
    static var ipAddress : String {

        get {
            var address : String?

            // Get list of all interfaces on the local machine:
            var ifaddr : UnsafeMutablePointer<ifaddrs>?
            guard getifaddrs(&ifaddr) == 0 else { return "" }
            guard let firstAddr = ifaddr else { return "" }

            // For each interface ...
            for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
                let interface = ifptr.pointee

                // Check for IPv4 or IPv6 interface:
                let addrFamily = interface.ifa_addr.pointee.sa_family

                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {

                    // Check interface name:
                    let name = String(cString: interface.ifa_name)

                    if  name == "en0" || name == "pdp_ip0"  {

                        // Convert interface address to a human readable string:
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                    &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
            return address ?? ""
        }
    }
    
    //MARK:- Currency Conversion
    
    class func convertToDollar(number : Double) -> String {
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.currency
        currencyFormatter.currencyCode = "USD"
        currencyFormatter.maximumFractionDigits = 0
        currencyFormatter.minimumFractionDigits = 0
        currencyFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        return currencyFormatter.string(from: NSNumber(value: number))!
    }
    
    class func convertToINR(number : Double) -> String {

        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.currency
        currencyFormatter.currencyCode = "INR"
        currencyFormatter.maximumFractionDigits = 0
        currencyFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        return currencyFormatter.string(from: NSNumber(value: number))!
    }
  
    //MARK:- Cell Model
    
    class func getCellModel(type : CellType) -> CellModel {
        
        let model = CellModel()
        model.cellType = type
        return model
    }
    
    class func getCellModel(placeholder:String,cellType:CellType = .none) -> CellModel {
        
        let model = CellModel()
        model.cellType = cellType
        model.placeholder = placeholder
        return model
    }
    
    //MARK:- Register Cell
    
    class func registerTableViewCustomCell(_ arrayNib: [String], tableView: UITableView) {
        for strCellName: String in arrayNib {
            tableView.register(UINib(nibName: strCellName, bundle: nil), forCellReuseIdentifier: strCellName)
        }
    }
    
    class func registerCollectionViewCustomCell(_ arrayNib: [String], collectionView: UICollectionView) {
        for strCellName: String in arrayNib {
            collectionView.register(UINib(nibName: strCellName, bundle: nil), forCellWithReuseIdentifier: strCellName)
        }
    }
    
    //MARK:- UILabel text Height & width
    
    class func getLabelHeight(constraintedWidth width: CGFloat, font: UIFont,text:String) -> CGFloat {
        
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.font = font
        label.sizeToFit()
        
        return label.frame.height
    }
    
    class func getLabelWidth(constraintedheight: CGFloat, font: UIFont,text:String) -> CGFloat {
        
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: .greatestFiniteMagnitude, height: constraintedheight))
        label.numberOfLines = 1
        label.text = text
        label.font = font
        label.sizeToFit()
        
        return label.frame.width
    }
    
    //MARK:- FileManager get directory

    class func getDocumentsDirectory() -> URL{
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    class func loadImage(_ imageName: String) -> UIImage {
        return UIImage(contentsOfFile: imageName) ?? #imageLiteral(resourceName: "image_Chat")
    }
    
    //MARK:- Show Alert View
    ///Show AlertView With ok buttuon
    class func showAlertView(title: String?, message: String?) {
      
        switch UIApplication.shared.applicationState {
            case .active:
                let alert = SmartAlertController(title: title ?? kAppName, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: StringConstants.ButtonConstant.kOk, style: .default, handler: nil))
                alert.presentInOwnWindow(animated: true, completion: nil)
            default:
                break
        }
    }
    
    class func showAlertWithButtonAction(title: String?,message:String,buttonTitle:String,onOKClick: @escaping () -> ()){
        switch UIApplication.shared.applicationState {
            case .active:
                let alert = SmartAlertController(title: title ?? kAppName, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { action in
                      onOKClick()
                }))
                alert.presentInOwnWindow(animated: true, completion: nil)
            default:
                break
        }
    }
   
    class func showAlertWithTwoButtonAction(title: String?,message:String,attributedMessage: NSAttributedString?,buttonTitle1:String,buttonTitle2:String,onButton1Click: @escaping () -> (),onButton2Click: @escaping () -> ()) {
        
        switch UIApplication.shared.applicationState {
            case .active:
                
                let alert = SmartAlertController(title: title ?? kAppName, message: message, preferredStyle: .alert)
                
                if attributedMessage != nil {
                    alert.setValue(attributedMessage, forKey: "attributedTitle")
                }
                alert.addAction(UIAlertAction(title: buttonTitle1, style: .default, handler: { action in
                    onButton1Click()
                }))
                alert.addAction(UIAlertAction(title: buttonTitle2, style: .default, handler: { action in
                    onButton2Click()
                }))
                alert.presentInOwnWindow(animated: true, completion: nil)
            default:
                break
        }
    }
    
    //MARK:- Call on number
    class func call(phoneNumber: String)
    {
        let trimPhone = phoneNumber.replacingOccurrences(of: " ", with: "")
        if  let url = URL(string: "tel://\(trimPhone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //MARK:- Open mail
    class func openEmail(email: String) {
        if let url = URL(string: "mailto:\(email)")  {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //MARK:- Open webView
    
    class func openWebView(link: String) {
        let vc = WebViewVC(nibName: "WebViewVC", bundle: nil)
        vc.link = link
        kAppDelegate.topViewController()?.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- Open URL in Safari
    
    class func openUrl(strUrl:String) {
        if let url = URL(string: strUrl) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options:[:], completionHandler: nil)
            }
        } else {
            self.showAlertView(title: "", message: "No link found")
        }
    }
    
    //MARK:- Change Color for Text as AttributedString
    
    class func changeStringColor(string : String, array : [String], colorArray : [UIColor], arrFont:[UIFont]) ->  NSAttributedString {
        
        let attrStr = NSMutableAttributedString(string: string)
        let inputLength = attrStr.string.count
        let searchString = array

        for i in 0...searchString.count-1 {
            
            let string  = searchString[i]
            let searchLength = string.count
            var range = NSRange(location: 0, length: attrStr.length)
            
            while (range.location != NSNotFound) {
                range = (attrStr.string as NSString).range(of: string, options: [], range: range)
                if (range.location != NSNotFound) {
                    
                    if colorArray.count > 0 {
                        attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: colorArray[0], range: NSRange(location: range.location, length: searchLength))
                    }
                    
                    if arrFont.count > 0 {
                        attrStr.addAttribute(NSAttributedString.Key.font, value: arrFont[0], range: NSRange(location: range.location, length: searchLength))
                    }
                    
                    range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
                    return attrStr
                }
            }
        }
        return NSAttributedString()
    }
    
    //MARK:- Open Google map with Direction

    static func openGoogleMapWithDirection(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            //open app
            let url = URL(string: "comgooglemaps://?saddr=\(source.latitude),\(source.longitude)&daddr=\(destination.latitude),\(destination.longitude)&directionsmode=driving")
            
            if  let url = url, UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        } else {
            //open in browser
            let url = URL(string: "https://www.google.com/maps/?saddr=\(source.latitude),\(source.longitude)&daddr=\(destination.latitude),\(destination.longitude)&directionsmode=driving")
            
            if  let url = url, UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    static func openAppleMapWithDirection(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        
        let source1 = MKMapItem(placemark: MKPlacemark(coordinate: source))
        source1.name = "Source"
        
        let destination1 = MKMapItem(placemark: MKPlacemark(coordinate: destination))
        destination1.name = "Destination"
        
        MKMapItem.openMaps(with: [source1, destination1], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
    
    //MARK:- Email validation
    
    class func isEmail(_ email: String) -> Bool {
        let emailRegEx: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"

        let regExPredicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return regExPredicate.evaluate(with: email.lowercased())
    }
}

class SmartAlertController : UIAlertController {

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let screenBounds = UIScreen.main.bounds
        self.view.center = screenBounds.center
    }
}
