//
//  PaddingLabel.swift
//  DemoApp
//
//  Created by Himanshu Gabani on 27/08/19.
//  Copyright © 2019 Himanshu Gabani. All rights reserved.
//

import UIKit

class PaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 0.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat = 0.0
    @IBInspectable var rightInset: CGFloat = 0.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
    
    public func isPendingTag(isPending : Bool){
        self.backgroundColor = isPending ?  UIColor(hexString: "FDF6F2") : UIColor(hexString: "2C9FD0").withAlphaComponent(0.06)
        self.textColor = isPending ?  UIColor(hexString: "D14300") : UIColor(hexString: "1E79A0")
    }
}
