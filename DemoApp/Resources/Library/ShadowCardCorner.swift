//
//  ShadowCard.swift
//  PTE
//
//  Created by CS-MacSierra on 14/09/17.
//  Copyright © 2017 CS-Mac-Mini. All rights reserved.
//

import UIKit

@IBDesignable

class ShadowCardCorner: UIView {

    @IBInspectable
    var viewCornerRadius: CGFloat = 6.0
    
    override func layoutSubviews() {
        
        layer.cornerRadius = viewCornerRadius
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 2
        layer.shadowOpacity = 5.0
    }
}
