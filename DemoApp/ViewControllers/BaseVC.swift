//
//  BaseVC.swift
//  DemoApp
//
//  Created by Himanshu Gabani on 03/07/21.
//  Copyright © 2020 Himanshu Gabani. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {

    var timerConnected : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    // MARK: - Background forground Notification handler
    
    @objc func appMovedToForeground() {
        print("App moved to ForeGround!")
    }
    
    @objc func appMovedToBackground() {
        print("App moved to BackGround!")
    }
    
}
