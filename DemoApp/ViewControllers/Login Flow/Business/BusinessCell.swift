//
//  SelectionCell.swift
//  UNOappFormsApp
//
//  Created by Chirag on 17/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class BusinessCell: UITableViewCell {

    @IBOutlet var lblSelectionName: UILabel!
    @IBOutlet var lblSelectionStatus : UILabel!
    @IBOutlet var imgArrowWork: UIImageView!
    @IBOutlet weak var vwMain: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCellData(model: ClsBusinesses){
        
        lblSelectionName.text = String(format: "%@", model.name)
        lblSelectionStatus.isHidden = false
        
        if(model.active){
            lblSelectionStatus.textColor = UIColor.init(hexString: "2eaf4d")
            lblSelectionStatus.text = "ACTIVE"
        }
        else{
            lblSelectionStatus.textColor = UIColor.init(hexString: "f73936")
            lblSelectionStatus.text = "INACTIVE"
        }
    }
    
    func setCellDataLocation(model: ClsLocationDetail){
        
        lblSelectionName.text = String(format: "%@", model.name)
        lblSelectionStatus.isHidden = false
        
        if(model.active){
            lblSelectionStatus.textColor = UIColor.init(hexString: "2eaf4d")
            lblSelectionStatus.text = "ACTIVE"
        }
        else{
            lblSelectionStatus.textColor = UIColor.init(hexString: "f73936")
            lblSelectionStatus.text = "INACTIVE"
        }
    }
}
