//
//  ViewController.swift
//  DemoApp
//
//  Created by Himanshu Gabani on 07/08/21.
//  Copyright © 2020 Himanshu Gabani. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
//        callAPIForGet(loader: true)
        // Do any additional setup after loading the view.
        lbl.text = DateFunctions.getCurrentDateInDashboardFormat()
    }
}

//MARK:- API Call
extension ViewController {
    func callAPIForGet(loader: Bool) {
        
        if loader {
            APIManager.shared.showIndicator()
        }
        
        let url = "\(APIManager.Constants.URLs.LOGIN_USER)"
        
        APIManager.shared.makeRequest(server: SERVER_URL, endPoint: url, method: .get,parameters: [:], encoding: JSONEncoding.default,headers: APIManager.shared.authHeaders).done { (response) in
            //Success
            print("succ")
        }.catch { error in
            //error
            print(error.localizedDescription)
            APIManager.shared.stopIndicator()
            CommonMethods.showAlertView(title: "", message: error.localizedDescription)
        }.finally {
            //API call complete
            APIManager.shared.stopIndicator()
        }
    }
}
