//
//  ColorConstant.swift
//  DemoApp
//
//  Created by Himanshu Gabani on 27/08/19.
//  Copyright © 2019 Himanshu Gabani. All rights reserved.
//

import UIKit

class ColorConstant: NSObject {
    static let TextMainColor                        = #colorLiteral(red: 0.168627451, green: 0.1725490196, blue: 0.1764705882, alpha: 1)
    static let SelectedTabColor                     = #colorLiteral(red: 0.8431372549, green: 0.8784313725, blue: 0.9960784314, alpha: 1)
    static let UnSelectedTabColor                   = #colorLiteral(red: 0.5921568627, green: 0.5960784314, blue: 0.6, alpha: 1)
    static let SelectedTopColor                     = #colorLiteral(red: 0.462745098, green: 0.6117647059, blue: 0.9882352941, alpha: 1)
    static let UnSelectedDetailColor                = #colorLiteral(red: 0.9176470588, green: 0.9215686275, blue: 0.9254901961, alpha: 1)
}
