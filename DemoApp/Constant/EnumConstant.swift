//
//  EnumConstant.swift
//  DemoApp
//
//  Created by Himanshu Gabani on 05/09/19.
//  Copyright © 2019 Himanshu Gabani. All rights reserved.
//

import UIKit

enum CellType {
    
    case none
}

enum StoryBoardName : String {
    case Main = "Main"
}

class Example {
    struct Method {
        static let MethodNew = "methodNew"
    }
}

class StoryBoard {
    struct Data {
        static let Main = UIStoryboard(name: StoryBoardName.Main.rawValue, bundle: .main)
    }
}
