
import Foundation

struct StringConstants {
    
    ///// DO NOT CHANGE ANY CONSTANT WITHOUT DISCUSSION OF TEAM ///////////
    
    struct ButtonConstant {
        
        static let kOk                                      = "Ok"
        static let kCancel                                  = "Cancel"
        static let kYes                                     = "Yes"
        static let kNo                                      = "No"
    }
    
    struct MessageConstant {
    
        static let LogoutMsg                                = "Are you sure you want to sign out?"
    }
    
    struct NoDataFound {
        
        static let NoItemFound                              = "No Items Found"
    }
}

let enter_email: String = "Please enter email"
let enter_valid_email: String = "Please enter valid email"
let enter_pasword: String = "Please enter password"

//MARK: Error
let ERROR_NOT_EXPECTED : String = "Unfortunately, we were not able to process your request. Please try again later."
let NULL_RESPONSE : String = "Unfortunately, we were not able to process your request. Please try again later."
let NO_INTERNET_CONNECTION : String = "There seems to be a connectivity issue. Please check your connection to the network and try again."
let REQUEST_TIMEOUT :String = "We ran into an issue getting you the information you requested. Please try again later."

