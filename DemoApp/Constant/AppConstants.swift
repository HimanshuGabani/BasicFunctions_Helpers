
import Foundation
import UIKit

//MARK: Global Constant
let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
let APP_VERSION: String = Bundle.main.releaseVersionNumber!
let APP_BUILD_NUMBER: String = Bundle.main.buildVersionNumber!
let APP_BUNDLE_IDENTIFIER = Bundle.main.bundleIdentifier
let APP_PLATFORM = "ios"
let KStatusBarHeight = UIApplication.shared.statusBarFrame.height

let kAppName = "DemoApp"

let NOTIFICATION_NAME_DEMO = NSNotification.Name(rawValue: "notificationDemo")

let SERVER_URL = "https://www.xyz.com"
let SERVER_API_TOKEN = ""

//MARK: AppConstant
struct AppConstants {
    
    struct UserDefaultKeys {
        
        static let UserInfoModelKey                 = "UserInfoModelKey"
    }
}

//MARK: Device Constant
class DeviceConstant{
    
    enum UIUserInterfaceIdiom : Int {
        case Unspecified
        case Phone
        case Pad
    }
    
    struct ScreenSize {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_X_OR_ABOVE          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
    }
}
