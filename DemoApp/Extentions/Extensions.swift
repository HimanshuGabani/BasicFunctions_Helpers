//
//  Extensions.swift
//  oma3.0
//
//  Created by Meghdoot on 28/05/18.
//  Copyright © 2018 Umakant. All rights reserved.
//

import Foundation
import UIKit

private var myCustomWindow: UIWindow!

extension UIAlertController {
    
    func presentInOwnWindow(animated: Bool, completion: (() -> Void)?) {
        myCustomWindow = UIWindow(frame: UIScreen.main.bounds)
        myCustomWindow.rootViewController = UIViewController()
        myCustomWindow.windowLevel = .alert + 1
        myCustomWindow.makeKeyAndVisible()
        myCustomWindow.rootViewController?.present(self, animated: animated, completion: completion)
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        myCustomWindow = nil
    }
}


extension UITextView {
    func numberOfLines() -> Int {
        if let fontUnwrapped = self.font {
            return Int(self.contentSize.height / fontUnwrapped.lineHeight)
        }
        return 0
    }
    
    func centerVertically() {
        let fittingSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fittingSize)
        let topOffset = (bounds.size.height - size.height * zoomScale) / 2
        let positiveTopOffset = max(0, topOffset)
        contentOffset.y = -positiveTopOffset
    }
}


extension UILabel {
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            } else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            attributedString.addAttribute(NSAttributedString.Key.kern,
                                          value: newValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            
            attributedText = attributedString
        }
        get {
            if let currentLetterSpace = attributedText?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            } else {
                return 0
            }
        }
    }
    
    func addCharacterSpacing() {
        if let labelText = text, labelText.count > 0 {
            let attributedString = NSMutableAttributedString(string: labelText)
            attributedString.addAttribute(NSAttributedString.Key.kern, value: 1.15, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
}

extension Float {
    var twoDigitValue: String {
        if self <= 0 {
            return "0.00"
        }
        let roundedValue = roundf(self * 1000000) / 1000000
        return String(format: "%.2f", roundedValue)
    }
    
    var clean: String {
        if self <= 0 {
            return "0.00"
        }
        let roundedValue = roundf(self * 1000000) / 1000000
        return self.truncatingRemainder(dividingBy:1) == 0 ? String(format: "%.0f", self) : String(format: "%.2f", roundedValue)
    }
}


extension Float64 {
    var twoDigitValue: String {
        if self <= 0 {
            return "0.00"
        }
        let roundedValue = roundf(Float(self * 1000000)) / 1000000
        return String(format: "%.2f", roundedValue)
    }
    
    var clean: String {
        if self <= 0 {
            return "0.00"
        }
        let roundedValue = roundf(Float(self * 1000000)) / 1000000
        return self.truncatingRemainder(dividingBy:1) == 0 ? String(format: "%.0f", self) : String(format: "%.2f", roundedValue)
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

extension Bundle {
    static func loadView<T>(fromNib name: String, withType type: T.Type) -> T {
        if let view = Bundle.main.loadNibNamed(name, owner: nil, options: nil)?.first as? T {
            return view
        }
        fatalError("Could not load view with type " + String(describing: type))
    }
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
    }
}

extension Dictionary {
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    
    func printJson() {
        print(json)
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

extension Double {
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func transportAmountFormat() -> String {
        let doubleValue = self
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .decimal
        formatter.roundingMode = .halfUp
        let str = formatter.string(from: NSNumber(value: doubleValue))
        return "\(str ?? "0.00")"
    }
    
    func storageAmountFormat() -> String {
        let doubleValue = self
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .decimal
        formatter.roundingMode = .halfUp
        let str = formatter.string(from: NSNumber(value: doubleValue))
        return "\(str ?? "0.00")"
    }
    
    func csvAmountFormat() -> String {
        let doubleValue = self
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .none
        formatter.roundingMode = .halfUp
        let str = formatter.string(from: NSNumber(value: doubleValue))
        return "$\(str ?? "0.00")"
    }
    
    func formattedValue() -> String {
        let doubleValue = self
        let formatter = NumberFormatter()
        formatter.currencyCode = "USD"
        formatter.currencySymbol = "$"
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .currencyAccounting
        formatter.roundingMode = .halfUp
        return formatter.string(from: NSNumber(value: doubleValue)) ?? "$\(doubleValue)"
    }
    
    func distanceValue() -> String {
        let doubleValue = self
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .decimal
        formatter.roundingMode = .halfUp
        return formatter.string(from: NSNumber(value: doubleValue)) ?? "0"
    }
    
    var fixTwoDigitValue: String {
        var finalString = self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
        if finalString.contains(".") {
            if finalString.components(separatedBy: ".").last?.count == 1 {
                finalString = finalString + "0"
            }
        }
        return finalString
    }
}

extension DateFormatter {
    func setLocale() {
        self.locale = Locale(identifier: NSLocale.current.identifier)
    }
}

extension String {
    func toDouble() -> Double? {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current // USA: Locale(identifier: "en_US")
        formatter.numberStyle = .decimal
        formatter.roundingMode = .halfUp
        let number = formatter.number(from: self)
        return number?.doubleValue
    }
    
    func cleanPhoneNo() -> String {
        var strPhoneNo : String = self
        strPhoneNo = strPhoneNo.trim()
        strPhoneNo = strPhoneNo.replacingOccurrences(of: "-", with: "")
        strPhoneNo = strPhoneNo.replacingOccurrences(of: " ", with: "")
        strPhoneNo = strPhoneNo.replacingOccurrences(of: "(", with: "")
        strPhoneNo = strPhoneNo.replacingOccurrences(of: ")", with: "")
        strPhoneNo = strPhoneNo.replacingOccurrences(of: "_", with: "")
        
        return strPhoneNo
    }
    
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.size.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.size.width)
    }
}

extension NSAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.size.height)
    }
    
    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.size.width)
    }
}

extension Float {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

public extension Sequence {
    func categorise<U : Hashable>(_ key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        var dict: [U:[Iterator.Element]] = [:]
        for el in self {
            let key = key(el)
            if case nil = dict[key]?.append(el) { dict[key] = [el] }
        }
        return dict
    }
}

extension FloatingPoint {
    var kFormatted: String {
        if self >=  100 {
            if self.truncatingRemainder(dividingBy: 100) == 0 {
                return String(format : "%.0fK",(self/100 as! CVarArg))
            } else {
                return String(format : "%.2fK",(self/100 as! CVarArg))
            }
        } else {
            return String(format : "%.0f",self as! CVarArg)
        }
    }
}

extension UITableView {
    func registerTableViewCustomCell(_ arrayNib: [String]) {
        for strCellName: String in arrayNib {
            self.register(UINib(nibName: strCellName, bundle: nil), forCellReuseIdentifier: strCellName)
        }
    }
}

extension UICollectionView {
    func registerCollectionViewCustomCell(_ arrayNib: [String]) {
        for strCellName: String in arrayNib {
            self.register(UINib(nibName: strCellName, bundle: nil), forCellWithReuseIdentifier: strCellName)
        }
    }
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

extension UIView {
    func loadNoDataFoundView(image:UIImage? = UIImage(named: "noStorageService") ,message:String) {
        self.window?.rootViewController?.view.endEditing(true)
        
        //remove if already there
        for vw in self.subviews{
            if let myvw = vw as? NoDataFoundView {
                myvw.removeFromSuperview()
            }
        }
        
        //add view
        let noDataFoundView = Bundle.main.loadNibNamed("NoDataFoundView", owner: self, options: nil)?[0] as! NoDataFoundView
        noDataFoundView.frame = CGRect.init(x: (self.frame.width / 2) - 80, y: (self.frame.height / 2) - 125, width: 160, height:250)
        //        noDataFoundView.center = self.center
        
        noDataFoundView.message = message
        noDataFoundView.image = image
        
        self.addSubview(noDataFoundView)
    }
    
    func removeNoDataFoundView() {
        for vw in self.subviews {
            if let myvw = vw as? NoDataFoundView {
                myvw.removeFromSuperview()
            }
        }
    }
}

extension UINavigationController {
    
    ///Get previous view controller of the navigation stack
    func previousViewController() -> UIViewController? {
        let lenght = self.viewControllers.count
        let previousViewController: UIViewController? = lenght >= 2 ? self.viewControllers[lenght-2] : nil
        return previousViewController
    }
    
    func popAllAndSwitch(vc:UIViewController) {
        setViewControllers([vc], animated: true)
    }
}


extension UIButton {
    func transformImage() {
        self.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        self.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        self.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
    }
}

extension UITextField : UITextFieldDelegate {
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text: NSString = (textField.text ?? "") as NSString
        let finalString = text.replacingCharacters(in: range, with: string)
        
        var lbl : UILabel?
        if let vw = textField.leftView as? UILabel {
            lbl = vw
        }
        if finalString.count > 0 {
            lbl?.textColor = self.textColor
        } else {
            let txt = self.attributedPlaceholder
            var color : UIColor?
            if let clr = txt?.attribute(NSAttributedString.Key.foregroundColor, at: 0, effectiveRange: nil) as? UIColor {
                color = clr
            }
            lbl?.textColor = color
        }
        return true
    }
    
    func setLeftCurrencySymbol(width:CGFloat = 10) {
        self.delegate = self
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: self.frame.height))
        label.font = self.font
        if self.text!.count > 0 {
            label.textColor = self.textColor
        } else {
            let txt = self.attributedPlaceholder
            var color : UIColor?
            if let clr = txt?.attribute(NSAttributedString.Key.foregroundColor, at: 0, effectiveRange: nil) as? UIColor {
                color = clr
            }
            label.textColor = color
        }
        label.text = "$ "
        self.leftView = label
        label.textAlignment = .center
        self.leftViewMode = .always
    }
    
    func addActionButton(title: String) {
        self.rightViewMode = .always
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 22, height: 18))
        btn.setTitle(title, for: .normal)
        btn.contentMode = .center
        self.rightView = btn
    }
    
    func addDropdownImage() {
        self.tintColor = .clear
        self.rightViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        let image = UIImage(named: "downArrow")
        imageView.contentMode = .center
        imageView.image = image
        imageView.clipsToBounds = true
        self.rightView = imageView
    }
    
    func addToolbar(target: Any, selector: Selector, isShowCancel:Bool = true) {
        let screenWidth = UIScreen.main.bounds.width
        
        //Add Tool Bar as input AccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        toolBar.barStyle = .default
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
        let doneBarButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
        var arrButton = [flexibleSpace, doneBarButton]
        if isShowCancel {
            arrButton.append(cancelBarButton)
        }
        toolBar.setItems(arrButton, animated: false)
        
        self.inputAccessoryView = toolBar
    }
    
    func addInputViewDatePicker(target: Any, selector: Selector) {
        
        let screenWidth = UIScreen.main.bounds.width
        
        //Add DatePicker as inputView
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        datePicker.datePickerMode = .date
        self.inputView = datePicker
        
        //Add Tool Bar as input AccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
        let doneBarButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
        toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)
        
        self.inputAccessoryView = toolBar
    }
    
    @objc func cancelPressed() {
        self.resignFirstResponder()
    }
    
    func addTimePickerView(target: Any, selector: Selector) {
        let screenWidth = UIScreen.main.bounds.width
        
        //Add DatePicker as inputView
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        datePicker.datePickerMode = .time
        //        datePicker.locale = Locale(identifier: "en_US")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "hh:mm a"
        if let date = dateFormatter.date(from: "00:00") {
            datePicker.date = date
        }
        
        self.inputView = datePicker
        
        //Add Tool Bar as input AccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
        let doneBarButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
        toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)
        
        self.inputAccessoryView = toolBar
    }
    
}

extension UIViewController {
    
    // MARK: - Push and pop view controller
    func pushViewController(storyboard:String,viewName:String,animation:Bool,isHideBottomTab:Bool=false) {
        let storyBoard = UIStoryboard(name: storyboard, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: viewName)
        vc.hidesBottomBarWhenPushed = isHideBottomTab
        self.navigationController?.pushViewController(vc, animated: animation)
    }
    
    func popViewController() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    /// pop back to specific viewcontroller
    func popBack<T: UIViewController>(toControllerType: T.Type) {
        if var viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            viewControllers = viewControllers.reversed()
            for currentViewController in viewControllers {
                if currentViewController .isKind(of: toControllerType) {
                    self.navigationController?.popToViewController(currentViewController, animated: true)
                    break
                }
            }
        }
    }
    
    func dismissViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    var previousViewController:UIViewController? {
        if let controllersOnNavStack = self.navigationController?.viewControllers, controllersOnNavStack.count >= 2 {
            let n = controllersOnNavStack.count
            return controllersOnNavStack[n - 2]
        }
        return nil
    }
    
    /// - Parameter child: Child view controller.
    func add(_ child: UIViewController, containerView : UIView) {
        addChild(child)
        child.view.frame = CGRect(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height)
        containerView.addSubview(child.view)
        self.addChild(child)
        child.didMove(toParent: self)
        child.view.layoutIfNeeded()
    }

    /// It removes the child view controller from the parent.
    func remove( containerView : UIView) {
        guard parent != nil else {
            return
        }
        willMove(toParent: nil)
        removeFromParent()
        containerView.removeFromSuperview()
    }
}

extension UIImage {
    
    func imageWithColor(tintColor: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        tintColor.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension Sequence {
    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        return Dictionary.init(grouping: self, by: key)
    }
}

extension UIImageView {
    
    func setImageForURL(urlStr: String, placeHolder: UIImage?) {
        
        if let url = URL(string: urlStr) {
            self.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.sd_setImage(with: url, placeholderImage: placeHolder)
        } else {
            
            self.image = placeHolder
        }
    }
}

// MARK: - UIViewController implementation

extension UIViewController {
    
    var isModal: Bool {
        
        let presentingIsModal = presentingViewController != nil
        let presentingIsNavigation = navigationController?.presentingViewController?.presentedViewController == navigationController
        let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController
        
        return presentingIsModal || presentingIsNavigation || presentingIsTabBar
    }
}

extension UIStackView {
    
    func safelyRemoveArrangedSubviews() {
        
        // Remove all the arranged subviews and save them to an array
        let removedSubviews = arrangedSubviews.reduce([]) { (sum, next) -> [UIView] in
            self.removeArrangedSubview(next)
            return sum + [next]
        }
        
        // Deactive all constraints at once
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
        
        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}

extension Array where Element == String {
    func getAddress() -> String{
        return self.filter ({ !$0.isEmpty }).joined(separator: ", ")
    }
}

extension UIViewController {
    
    static func addGlobalViewWillAppear() {
        if self != UIViewController.self {
            return
        }
        let _: () = {
            let originalSelector = #selector(UIViewController.viewWillAppear(_:))
            let swizzledSelector = #selector(UIViewController.global_viewWillAppear(_:))
            let originalMethod = class_getInstanceMethod(self, originalSelector)
            let swizzledMethod = class_getInstanceMethod(self, swizzledSelector)
            method_exchangeImplementations(originalMethod!, swizzledMethod!);
        }()
    }
    
    @objc func global_viewWillAppear(_ animated: Bool) {
        print(">>>>>>\(self)")
    }
}

extension Int {
    var cleanValue: String {
        return String(format: "%d", self)
    }
    
    var toString: String {
        return String(format:"%d", self)
    }
}

extension Data {
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func dataToFile(fileName: String) -> NSURL? {
        
        // Make a constant from the data
        let data = self
        
        // Make the file path (with the filename) where the file will be loacated after it is created
        let filePath = getDocumentsDirectory().appendingPathComponent(fileName)
        
        do {
            // Write the file from data into the filepath (if there will be an error, the code jumps to the catch block below)
            try data.write(to: URL(fileURLWithPath: filePath))
            
            // Returns the URL where the new file is located in NSURL
            return NSURL(fileURLWithPath: filePath)
            
        } catch {
            // Prints the localized description of the error from the do block
            print("Error writing the file: \(error.localizedDescription)")
        }
        
        // Returns nil if there was an error in the do-catch -block
        return nil
    }
}

public extension UIAlertController {
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        win.windowLevel = UIWindow.Level.alert + 1  // Swift 3-4: UIWindowLevelAlert + 1
        win.makeKeyAndVisible()
        vc.present(self, animated: true, completion: nil)
    }
}

extension CGRect {
    var center : CGPoint {
        return CGPoint(x:self.midX, y:self.midY)
    }
}

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}

extension UITextField {
   @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}

//extension UIView {
//    func addCustomNavBar(title:String? = nil) -> CustomNavigationBarVw {
//        //remove if already there
//        for vw in self.subviews {
//            if let myvw = vw as? CustomNavigationBarVw {
//                myvw.removeFromSuperview()
//            }
//        }
//
//        //add view
//        let navBar = Bundle.main.loadNibNamed("CustomNavigationBarVw", owner: self, options: nil)?[0] as! CustomNavigationBarVw
//        navBar.title = title
//        navBar.frame = CGRect.init(x: 0, y: UIApplication.shared.statusBarFrame.height, width: DeviceConstant.ScreenSize.SCREEN_WIDTH, height:60)
//
//        let status = UIView(frame: CGRect(x: 0, y: 0, width: DeviceConstant.ScreenSize.SCREEN_WIDTH, height: UIApplication.shared.statusBarFrame.height))
//        status.backgroundColor = UIColor(hexString: "F7F7F7")
//        self.addSubview(status)
//
//        self.addSubview(navBar)
//
//        return navBar
//    }
//}


extension UILabel {

    func startBlink() {
        UIView.animate(withDuration: 0.8,
              delay:0.0,
              options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
              animations: { self.alpha = 0 },
              completion: nil)
    }

    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}

extension UILabel {
    func addCharactersSpacing(spacing:CGFloat, text:String) {
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSMakeRange(0, text.count))
        self.attributedText = attributedString
    }
}

//MARK: - UIStoryBoard Extension & Protocol

extension UIViewController {

    class func instantiate<T: UIViewController>(appStoryboard: StoryBoardName) -> T {

        let storyboard = UIStoryboard(name: appStoryboard.rawValue, bundle: nil)
        let identifier = String(describing: self)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! T
    }
}

extension UIButton {
    func addRightImage(image: UIImage, offset: CGFloat) {
        self.setImage(image, for: .normal)
        self.imageView?.translatesAutoresizingMaskIntoConstraints = false
        self.imageView?.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0.0).isActive = true
        self.imageView?.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -offset).isActive = true
    }
}
