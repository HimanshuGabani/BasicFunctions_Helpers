//
//  StringClasses.swift
//
//  Created by Apple on 11/10/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import Foundation

import UIKit

public extension String {
    
    var length: Int {
        return self.count
    }
    
    var pathExtension: String {
        get {
            return (self as NSString).pathExtension
        }
    }
    
    func stringByAppendingPathComponent(_ path: String) -> String {
        let string = self as NSString
        
        return string.appendingPathComponent(path)
    }
    
    func hasString(_ string: String, caseSensitive: Bool = true) -> Bool {
        if caseSensitive {
            return self.range(of: string) != nil
        } else {
            return self.lowercased().range(of: string.lowercased()) != nil
        }
    }
    
    static func encodeToBase64(_ string: String) -> String {
        let data: Data = string.data(using: String.Encoding.utf8)!
        return data.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    }
    
    static func decodeBase64(_ string: String) -> String {
        let data: Data = Data(base64Encoded: string as String, options: NSData.Base64DecodingOptions(rawValue: 0))!
        return NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
    }
    
    func encodeToBase64() -> String {
        return String.encodeToBase64(self)
    }
    
    func decodeBase64() -> String {
        return String.decodeBase64(self)
    }
    
    func convertToNSData() -> Data {
        return self.data(using: String.Encoding.utf8)!
    }
    
    func URLEncode() -> String {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    }
    
    func removeWhiteSpacesFromString() -> String {
        let trimmedString: String = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimmedString
    }
    
    func trim() -> String {
        let str=self.trimmingCharacters(in: .whitespaces)
        return str
    }
    
    func insert(string:String,ind:Int) -> String {
        return  String(self.prefix(ind)) + string + String(self.suffix(self.count-ind))
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    func convertToLocalTimeZone() -> Date {
        let formatter = DateFormatter()
        formatter.setLocale()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = NSTimeZone.local
        let dt = formatter.date(from: self)
        return dt!
    }
    
    func convertToLocalTimeZoneAMPM() -> String {
        let formatter = DateFormatter()
        formatter.setLocale()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = NSTimeZone.local
        let dt = formatter.date(from: self)
        
        let formatter1 = DateFormatter()
        formatter1.setLocale()
        formatter1.dateFormat = "yyyy-MM-dd hh:mm a"
        let str = formatter1.string(from: dt!)
        return str
    }
    
    func convertToDateOrderPickupTime(fromTimeZone : String = "",toTimeZone : String = "" ) -> Date {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.setLocale()
        if fromTimeZone.length > 1 {
            formatter.timeZone = NSTimeZone(name: fromTimeZone) as TimeZone?
        }
        let dt = formatter.date(from: self)
        
        if dt != nil {
            let formatter1 = DateFormatter()
            formatter1.setLocale()
            if toTimeZone.length > 1 {
                formatter1.timeZone = NSTimeZone(name: toTimeZone) as TimeZone?
            }
            formatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let str = formatter1.string(from: dt!)
            let date = formatter1.date(from: str)
            return date!
        } else {
            return Date()
        }
    }
}
