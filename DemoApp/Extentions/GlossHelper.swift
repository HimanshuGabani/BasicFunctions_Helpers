//
//  GlossHelper.swift
//  GlossModel
//
//  Created by Meghdoot on 17/11/17.
//  Copyright © 2017 Meghdoot. All rights reserved.
//


extension UserDefaults {
    class func saveGlossArray<T : Glossy>(data array : [T], key : String) {
        UserDefaults.standard.setValue(array.toJSONArray(), forKey: key)
    }
    
    class func getGlossArray<T: Glossy>(type : T.Type, withKey key: String) -> [T] {
        var arrTmp = [T]()
        for obj in UserDefaults.standard.array(forKey: key)! {
            arrTmp.append(T.init(json: obj as! JSON)!)
        }
        return arrTmp
    }
    
    class func saveGlossObject<T : Glossy>(data object : T, key : String) {
        UserDefaults.standard.setValue(object.toJSON(), forKey: key)
    }
    
    class func getGlossObject<T: Glossy>(type : T.Type, withKey key: String) -> T {
        if UserDefaults.standard.object(forKey: key) != nil {
            let objGloss = T(json: UserDefaults.standard.value(forKey: key) as! JSON)
            return objGloss!
        }
        return T(json: [:])!
    }
    
    class func saveCustomObject<T : NSObject & Codable>(object : T, withKey key: String) {
        if let encoded = try? JSONEncoder().encode(object) {
            UserDefaults.standard.set(encoded, forKey: key)
        }
    }
    
    class func getCustomObject<T : NSObject & Codable>(type: T.Type, withKey key: String) -> T {
        if let userData = UserDefaults.standard.data(forKey: key),
            let user = try? JSONDecoder().decode(type, from: userData) {
            return user
        }
        return T()
    }
    
}
