import Foundation
public extension FileManager {
    // MARK: - Enums -
    /**
     Directory type enum
     */
    enum DirectoryType : Int {
        case mainBundle
        case library
        case documents
        case cache
    }
    
    // MARK: - Class functions -
    
    /**
     Read a file an returns the content as String
     */
    static func readTextFile(_ file: String, ofType: String) throws -> String? {
        return try String(contentsOfFile: Bundle.main.path(forResource: file, ofType: ofType)!, encoding: String.Encoding.utf8)
    }
    
    /**
     Save a given array into a PLIST with the given filename
     */
    static func saveArrayToPath(_ directory: DirectoryType, filename: String, array: Array<AnyObject>) -> Bool {
        var finalPath: String
        
        switch directory {
        case .mainBundle:
            finalPath = self.getBundlePathForFile("\(filename).plist")
        case .library:
            finalPath = self.getLibraryDirectoryForFile("\(filename).plist")
        case .documents:
            finalPath = self.getDocumentsDirectoryForFile("\(filename).plist")
        case .cache:
            finalPath = self.getCacheDirectoryForFile("\(filename).plist")
        }
        
        return NSKeyedArchiver.archiveRootObject(array, toFile: finalPath)
    }
    
    /**
     Load array from a PLIST with the given filename
     */
    static func loadArrayFromPath(_ directory: DirectoryType, filename: String) -> AnyObject? {
        var finalPath: String
        
        switch directory {
        case .mainBundle:
            finalPath = self.getBundlePathForFile(filename)
        case .library:
            finalPath = self.getLibraryDirectoryForFile(filename)
        case .documents:
            finalPath = self.getDocumentsDirectoryForFile(filename)
        case .cache:
            finalPath = self.getCacheDirectoryForFile(filename)
        }
        
        return NSKeyedUnarchiver.unarchiveObject(withFile: finalPath) as AnyObject?
    }
    
    /**
     Get the Bundle path for a filename
     */
    static func getBundlePathForFile(_ file: String) -> String {
        let fileExtension = file.pathExtension
        return Bundle.main.path(forResource: file.replacingOccurrences(of: String(format: ".%@", file), with: ""), ofType: fileExtension)!
    }
    
    /**
     Get the Documents directory for a filename
     */
    static func getDocumentsDirectoryForFile(_ file: String) -> String {
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        return documentsDirectory.stringByAppendingPathComponent(String(format: "%@/", file))
    }
    
    /**
     Get the Library directory for a filename
     */
    static func getLibraryDirectoryForFile(_ file: String) -> String {
        let libraryDirectory = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0]
        return libraryDirectory.stringByAppendingPathComponent(String(format: "%@/", file))
    }
    
    /**
     Get the Cache directory for a filename
     */
    static func getCacheDirectoryForFile(_ file: String) -> String {
        let cacheDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
        return cacheDirectory.stringByAppendingPathComponent(String(format: "%@/", file))
    }
    
    /**
     Returns the size of the file
     */
    static func fileSize(_ file: String, fromDirectory directory: DirectoryType) throws -> NSNumber? {
        if file.count != 0 {
            var path: String
            
            switch directory {
            case .mainBundle:
                path = self.getBundlePathForFile(file)
            case .library:
                path = self.getLibraryDirectoryForFile(file)
            case .documents:
                path = self.getDocumentsDirectoryForFile(file)
            case .cache:
                path = self.getCacheDirectoryForFile(file)
            }
            
            if FileManager.default.fileExists(atPath: path) {
                let fileAttributes: NSDictionary? = try FileManager.default.attributesOfItem(atPath: file) as NSDictionary?
                if let _fileAttributes = fileAttributes {
                    return NSNumber(value: _fileAttributes.fileSize() as UInt64)
                }
            }
        }
        return nil
    }
    
    /**
     Delete a file with the given filename
     */
    static func deleteFile(_ file: String, fromDirectory directory: DirectoryType) throws -> Bool {
        if file.count != 0 {
            var path: String
            
            switch directory {
            case .mainBundle:
                path = self.getBundlePathForFile(file)
            case .library:
                path = self.getLibraryDirectoryForFile(file)
            case .documents:
                path = self.getDocumentsDirectoryForFile(file)
            case .cache:
                path = self.getCacheDirectoryForFile(file)
            }
            
            if FileManager.default.fileExists(atPath: path) {
                do {
                    try FileManager.default.removeItem(atPath: path)
                    return true
                } catch {
                    return false
                }
            }
        }
        
        return false
    }
    
    /**
     Move a file from a directory to another
     */
    static func moveLocalFile(_ file: String, fromDirectory origin: DirectoryType, toDirectory destination: DirectoryType, withFolderName folderName: String? = nil) throws -> Bool {
        var originPath: String
        
        switch origin {
        case .mainBundle:
            originPath = self.getBundlePathForFile(file)
        case .library:
            originPath = self.getLibraryDirectoryForFile(file)
        case .documents:
            originPath = self.getDocumentsDirectoryForFile(file)
        case .cache:
            originPath = self.getCacheDirectoryForFile(file)
        }
        
        var destinationPath: String = ""
        if folderName != nil {
            destinationPath = String(format: "%@/%@", destinationPath, folderName!)
        } else {
            destinationPath = file
        }
        
        switch destination {
        case .mainBundle:
            destinationPath = self.getBundlePathForFile(destinationPath)
        case .library:
            destinationPath = self.getLibraryDirectoryForFile(destinationPath)
        case .documents:
            destinationPath = self.getDocumentsDirectoryForFile(destinationPath)
        case .cache:
            destinationPath = self.getCacheDirectoryForFile(destinationPath)
        }
        
        if folderName != nil {
            let folderPath: String = String(format: "%@/%@", destinationPath, folderName!)
            if !FileManager.default.fileExists(atPath: originPath) {
                try FileManager.default.createDirectory(atPath: folderPath, withIntermediateDirectories: false, attributes: nil)
            }
        }
        
        var copied: Bool = false, deleted: Bool = false
        if FileManager.default.fileExists(atPath: originPath) {
            do {
                try FileManager.default.copyItem(atPath: originPath, toPath: destinationPath)
                copied = true
            } catch {
                copied = false
            }
        }
        
        if destination != .mainBundle {
            if FileManager.default.fileExists(atPath: originPath) {
                do {
                    try FileManager.default.removeItem(atPath: originPath)
                    deleted = true
                } catch {
                    deleted = false
                }
            }
        }
        
        if copied && deleted {
            return true
        }
        return false
    }

    /**
     Move a file from a directory to another
     - returns: Returns true if the operation was successful, otherwise false
     */
    //    @available(*, obsoleted: 1.2.0, message: "Use moveLocalFile(_, fromDirectory:, toDirectory:, withFolderName:)")
    static func moveLocalFile(_ file: String, fromDirectory origin: DirectoryType, toDirectory destination: DirectoryType) throws -> Bool {
        return try self.moveLocalFile(file, fromDirectory: origin, toDirectory: destination, withFolderName: nil)
    }
    
    /**
     Duplicate a file into another directory
     */
    static func duplicateFileAtPath(_ origin: String, toNewPath destination: String) -> Bool {
        if FileManager.default.fileExists(atPath: origin) {
            do {
                try FileManager.default.copyItem(atPath: origin, toPath: destination)
                return true
            } catch {
                return false
            }
        }
        return false
    }
    
    /**
     Rename a file with another filename
     - returns: Returns true if the operation was successful, otherwise false
     */
    static func renameFileFromDirectory(_ origin: DirectoryType, atPath path: String, withOldName oldName: String, andNewName newName: String) -> Bool {
        var originPath: String
        
        switch origin {
        case .mainBundle:
            originPath = self.getBundlePathForFile(path)
        case .library:
            originPath = self.getLibraryDirectoryForFile(path)
        case .documents:
            originPath = self.getDocumentsDirectoryForFile(path)
        case .cache:
            originPath = self.getCacheDirectoryForFile(path)
        }
        
        if FileManager.default.fileExists(atPath: originPath) {
            let newNamePath: String = originPath.replacingOccurrences(of: oldName, with: newName)
            do {
                try FileManager.default.copyItem(atPath: originPath, toPath: newNamePath)
                do {
                    try FileManager.default.removeItem(atPath: originPath)
                    return true
                } catch {
                    return false
                }
            } catch {
                return false
            }
        }
        return false
    }

    //- returns: Returns true if the operation was successful, otherwise false
    static func setSettings(_ settings: String, object: AnyObject, forKey objKey: String) -> Bool {
        var path: String = self.getLibraryDirectoryForFile("")
        path = path + "/Preferences/"
        path = path + "\(settings)-Settings.plist"
        
        var loadedPlist: NSMutableDictionary
        if FileManager.default.fileExists(atPath: path) {
            loadedPlist = NSMutableDictionary(contentsOfFile: path)!
        } else {
            loadedPlist = NSMutableDictionary()
        }
        
        loadedPlist[objKey] = object
        
        return loadedPlist.write(toFile: path, atomically: true)
    }

    //- returns: Returns true if the operation was successful, otherwise false
    static func setAppSettingsForObject(_ object: AnyObject, forKey objKey: String) -> Bool {
        let APP_NAME: String = Bundle.main.infoDictionary!["CFBundleName"] as! String
        return self.setSettings(APP_NAME, object: object, forKey: objKey)
    }

    //returns: Returns string path of document directory
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
}
