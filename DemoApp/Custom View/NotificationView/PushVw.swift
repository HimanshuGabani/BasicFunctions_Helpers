//
//  CustomNavigationBarVw.swift
//  DemoApp
//
//  Created by Himanshu Gabani on 15/04/21.
//  Copyright © 2020 Himanshu Gabani. All rights reserved.
//

import UIKit

class PushVw: UIView {

    //MARK: IBOutlets
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var vwContent: UIView!
    
    var btnOkClickHandler: (() -> Void)?

    //MARK: View Life Cycle
    override func awakeFromNib() {
        
    }
    
    override func layoutSubviews() {
        
    }
        
    //MARK: IBAction
    @IBAction func btnOKClicked(_ sender: Any) {
        if btnOkClickHandler != nil {
            self.btnOkClickHandler!()
        }
    }
}
