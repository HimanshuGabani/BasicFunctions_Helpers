//
//  NoDataFoundView.swift
//  DemoApp
//
//  Created by Himanshu Gabani on 29/08/19.
//  Copyright © 2019 Himanshu Gabani. All rights reserved.
//

import UIKit

class NoDataFoundView: UIView {

    @IBOutlet var imgNoData: UIImageView!
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var width: NSLayoutConstraint!
    @IBOutlet var height: NSLayoutConstraint!
    @IBOutlet weak var topLabel: NSLayoutConstraint!
    
    var message = ""
    var image : UIImage?
    
    override func awakeFromNib() {
        
    }
    
    override func layoutSubviews() {
        
        lblMessage.text = message
        imgNoData.image = image
        
        if image == nil {
            width.constant = 0
            height.constant = 0
            topLabel.constant = 0
        }
    }
}

