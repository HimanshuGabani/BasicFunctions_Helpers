//
//  WebViewVC.swift
//  DemoApp
//
//  Created by Himanshu Gabani on 04/11/19.
//  Copyright © 2019 Himanshu Gabani. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController {

    //MARK: IBOutlets
    @IBOutlet var webView: WKWebView!
    @IBOutlet var loader: UIActivityIndicatorView!
    
    //MARK: Variables
    var link = ""
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }

    //MARK: Private Methods
    private func initialConfig() {
        webView.navigationDelegate = self
        
        if let url = URL(string: link.removeWhiteSpacesFromString()) {
            webView.load(URLRequest(url: url))
            loader.startAnimating()
        } else {
            self.webView.loadNoDataFoundView(message: "URL is not valid")
        }
    }
    
    //MARK: Action
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
}

//Webview Delegates
extension WebViewVC : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loader.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        loader.stopAnimating()
    }
}
